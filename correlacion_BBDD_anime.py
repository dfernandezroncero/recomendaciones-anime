import pandas as pd
#Leemos datos
def calculoCorrelacion():
    r_cols = ['user_id', 'anime_id', 'rating']
    rating = pd.read_csv('C:\\www\\recomendaciones-anime\\rating.csv', sep=',', names=r_cols, usecols=range(3), encoding="ISO-8859-1")
    rating=rating[rating['rating']!=-1]
    m_cols = ['anime_id', 'title']
    anime = pd.read_csv('C:\\www\\recomendaciones-anime\\anime.csv', sep=',', names=m_cols, usecols=range(2), encoding="ISO-8859-1")

    ratings = pd.merge(anime, rating)       
    #pivotamos la tabla con id usuario por el nombre del anime

    userRatings = ratings[:4000000].pivot_table(index=['user_id'],columns=['title'],values='rating')

    #Hacemos la correlación con person
    corrMatrix = userRatings.corr(method='pearson', min_periods=100)
    #print(corrMatrix)
    return corrMatrix.to_json (r'C:\www\recomendaciones-anime\CorrMatrix.json',orient='index')

    

