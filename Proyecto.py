import random


def comprueba_valor(texto):
    flag = True
    while flag:
        try:
            result = int(input(texto))
            flag = False
        except:
            print("Error, introduzca un entero ")
    return result


class Bus:
    def __init__(self, id, plazas):
        self.__id = id
        self.__plazas = plazas
        self.__listaBilletes = []

    def setPlazas(self, plaza):
        self.__plazas = plaza

    def getPlazas(self):
        return self.__plazas

    def getPlazasOcupadas(self):
        return len(self.__listaBilletes)

    def setId(self, id):
        self.__id = id

    def getId(self):
        return self.__id

    def insertarBillete(self, billete):
        if (self.getPlazasOcupadas() < self.__plazas):
            self.__listaBilletes.append(billete)
            return True
        else:
            return False

    def eliminarBillete(self, indiceBillete):
        self.__listaBilletes.remove(self.__listaBilletes[indiceBillete])

    def getListaBilletes(self):
        return self.__listaBilletes

    def venta(self, Propietario):
        self.__listaBilletes.append(Billete(Propietario, self.__id))

    def devolucion(self, indice):
        self.__listaBilletes.pop(indice)

    def __str__(self):
        return "Bus Num." + str(self.__id) + " Plazas Totales: " + str(self.__plazas) + " Plazas Ocupadas: " + str(
            self.getPlazasOcupadas())


class Billete:

    def __init__(self, propietario, idBus):
        self.__propietario = propietario
        self.__idBus = idBus

    def getPropietario(self):
        return self.__propietario

    def getIdBus(self):
        return self.__idBus

    def setPropietario(self, nPropietario):
        self.__propietario = nPropietario

    def setIdBus(self, nIdBus):
        self.__idBus = nIdBus

    def __str__(self):
        return "Propietario: " + self.__propietario + " Bus Num." + str(self.__idBus)


class BusManager:

    def __init__(self):
        self.__listaBuses = []

    def setListaBuses(self, Lista):
        self.__listaBuses = Lista

    def menuPrincipal(self):

        opcion = 0
        while opcion != 5:
            print("-----Menú-----\
                    \n 1. Venta de billetes\
                    \n 2. Devolución de billetes\
                    \n 3. Estado\
                    \n 4. Añadir buses\
                    \n 5. Salir")
            opcion = comprueba_valor("Elija una opción: ")
            if opcion == 1:
                self.ventaPlazaBus()
            elif opcion == 2:
                self.devolver()
            elif opcion == 3:
                self.listarBuses()
            elif opcion == 4:
                self.anadirBus()
            elif opcion == 5:
                print("Saliendo")
            else:
                print("Error. Introduzca otra opción")

    def listarBuses(self):
        if (len(self.__listaBuses) > 0):
            print("******** LISTA DE BUS *********")
            for bus in range(0, len(self.__listaBuses)):
                print(str(bus), ".", self.__listaBuses[bus])
        else:
            print("Todavía no hay ningún bus en el sistema.")

    def listarBilletes(self, idBus):
        if (len(self.__listaBuses[idBus].getListaBilletes())) > 0:
            print("******** LISTA DE BILLETES *********")
            for indiceBillete in range(0, len(self.__listaBuses[idBus].getListaBilletes())):
                print(indiceBillete, ". ", self.__listaBuses[idBus].getListaBilletes()[indiceBillete])
        else:
            print("Este bus todavía no tiene billetes vendidos.")

    def anadirBus(self):

        plazasNuevoBus = comprueba_valor("Introduzca las plazas que tiene el nuevo bus: ")
        idNuevoBus = self.llenarID()
        nuevoBus = Bus(idNuevoBus, plazasNuevoBus)
        self.__listaBuses.append(nuevoBus)
        print("Nuevo bus insertado con éxito.")

    def ventaPlazaBus(self):
        opcion = 0
        while (opcion != len(self.__listaBuses)):
            self.listarBuses()
            print(len(self.__listaBuses), "Volver")
            opcion = comprueba_valor("Elija un bus o salga: ")

            if 0 <= opcion <= len(self.__listaBuses) - 1:
                nombreUs = input("Ingrese el nombre del usuario: ")
                billeteUS = Billete(nombreUs, self.__listaBuses[opcion].getId())

                exito = self.__listaBuses[opcion].insertarBillete(billeteUS)
                if (exito):
                    print("Billete adquirido con éxito.")
                else:
                    print("La compra del billete ha fallado. Vuelve a intentarlo.")

                self.listarBilletes(opcion)
            elif opcion == len(self.__listaBuses):
                print("Volviendo")
            else:
                print("Error. Introduzca otra opción ")

    def devolver(self):
        opcion = 0
        while (opcion != len(self.__listaBuses)):
            self.listarBuses()
            print(len(self.__listaBuses), ".", "Volver")
            opcion = comprueba_valor("¿De qué bus desea devolver billetes?: ")
            if 0 <= opcion <= len(self.__listaBuses) - 1:
                self.listarBilletes(opcion)
                print(len(self.__listaBuses[opcion].getListaBilletes()), ".", "Volver")
                opcionBilletes = comprueba_valor("¿Qué billete quieres devolver?: ")
                if 0 <= opcionBilletes <= len(self.__listaBuses[opcion].getListaBilletes()) - 1:
                    self.__listaBuses[opcion].eliminarBillete(opcionBilletes)
                else:
                    print("Error. Introduzca otra opción.")
            elif opcion == len(self.__listaBuses):
                print("Volviendo al menú principal...")
            else:
                print("Error. Introduzca otra opción.")

    def llenarID(self):
        i = 0
        valor = random.randint(100, 999)
        while i < len(self.__listaBuses):
            if self.__listaBuses[i].getId() == valor:
                valor = random.randint(100, 999)
                i = 0
            else:
                i += 1
        return valor


busManager = BusManager()
busManager.menuPrincipal()
