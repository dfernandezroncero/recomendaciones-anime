import pandas as pd
from correlacion_BBDD_anime import *
from funsion_recomendacion_usuario import *
from flask import Flask, jsonify, request as req
import json
from pandas.io.json import json_normalize
from pandas import DataFrame

app = Flask(__name__)

@app.route("/cargaBBDD/", methods=["GET"]) #anotaciones en Flask, información que se pasa a la funsión
def cargaCorr():
    correlacion=calculoCorrelacion()
    return correlacion

@app.route("/recomendacion/", methods=["POST"])
def recomendacionUsuario1():
    animes= req.args.get('animes')
    corrMatrix=req.args.get('correlacion')
    userRatings=req.args.get('rankings')
    corrMatrix = json.loads(corrMatrix)
    corrMatrix = DataFrame(corrMatrix)
    recomendacion= recomendacionUsuario(animes,userRatings,corrMatrix)
    return recomendacion


@app.route("/guardar/", methods=["POST"])
def cargarDatos():
    userid = 1
    ranksConvertdo = []
    titleConvertdo = []
    title = req.form["titleE"]
    ranks = req.form["rankingE"]
    
    corrMatrix =json.loads(open('C:\\www\\recomendaciones-anime\\CorrMatrix.json').read())
    corrMatrix = DataFrame(corrMatrix)

    ranks = ranks.split(";")
    for i in range(0, len(ranks)-1):
         ranksConvertdo.append(float(ranks[i]))

    title = title.split(";")
    for i in range(0, len(title)-1):
         titleConvertdo.append(title[i])

    userRatings = [titleConvertdo, ranksConvertdo]
    recomendacion= recomendacionUsuario(userRatings[0],userRatings[1],corrMatrix)
    
    return jsonify({"about": recomendacion })






        
        
