import pandas as pd
from correlacion_BBDD_anime import *
import json
from pandas.io.json import json_normalize
def recomendacionUsuario(anime,ratingsAnime,corrMatrix):
    #Ahora vamos a producir recomendaciones para el usuario ID 0, añadido manualmente al dataset.
    #Le gusta Star Wars y The Empire Strikes Back, pero odia Gone with the Wind
    #myRatingsPrueba=['Kimi no Na wa.']
    #labels = [7.0]
    myRatings = pd.Series(ratingsAnime, index = anime)

    

    #myRatings = userRatings.loc[userid].dropna()
    print (type(myRatings))
    print('\nRating del Usuario 1:\n')
    print(myRatings)

    #Para cada película calificada vamos a recuperar la lista de películas similares de la matriz de correlación.
    #Luego se escala el puntaje de correlación según la calificación dada por el usuario en la película que es similar, entonces las películas que le gustaron mas van a contar más que las que no.

    simCandidates = pd.Series()
    #print(simCandidates)
    for i in range(0, len(myRatings.index)):
        #print ("Añadiendo pelis similares a " + myRatings.index[i] + "...")
        # Recuperar las pelis similares a las calificadas
        sims = corrMatrix[myRatings.index[i]].dropna()
        # Escalar la similaridad multiplicando por la calificación de la persona
        sims = sims.map(lambda x: x * myRatings[i])
        # Añadir el puntaje a la lista de candidatos similares
        simCandidates = simCandidates.append(sims)
        
    #Mirar los resultados:
    #print ("ordenando...")
    simCandidates.sort_values(inplace = True, ascending = False)
    #print('\nTabla De Recomenaciones:\n')
    #print (simCandidates.head(10))

    #Algunos animes aparecen más de una vez porque son similares a más de una de las calificadas.
    #Las agrupamos usando groupby() para sumar sus puntajes, así contarán más:

    simCandidates = simCandidates.groupby(simCandidates.index).sum()
    simCandidates.sort_values(inplace = True, ascending = False)
    #print('\nTabla De Recomenaciones Agrupando las calificaciones iguales :\n')
    #print(simCandidates.head(10))
    #La última cosa por hacer es quitar las películas ya calificadas:
    filteredSims = simCandidates.drop(myRatings.index)
    #print('\nTabla De Recomenaciones Quitando las peliculas Calificadas Por el Usuario :\n')
    return filteredSims.head().to_json()

